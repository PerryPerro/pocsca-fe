import React from 'react';
import { Card, CardMedia, CardContent, Typography, Link } from '@mui/material';

interface ContactCardProps {
  image: string;
  name: string;
  role: string;
  email: string;
}

const ContactCard: React.FC<ContactCardProps> = ({
  image,
  name,
  role,
  email,
}) => {
  return (
    <Card>
      <CardMedia component="img" height="300" image={image} alt={name} />
      <CardContent>
        <Typography variant="h5" component="div">
          {name}
        </Typography>
        <Typography color="text.secondary" gutterBottom>
          {role}
        </Typography>
        <Typography color="text.secondary" gutterBottom>
          <Link href={`mailto:${email}`}>{email}</Link>
        </Typography>
      </CardContent>
    </Card>
  );
};

export default ContactCard;

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../../styles/AverageDataDashboard.css';
// import SimpleLineGraph from '../ECMChart/ECMtestChart'; // Import the ECMLineChart component
import { SimpleGraph } from '../Charts/ECMtestChart'; // Import the ECMLineChart component
import { TemperatureGraph } from '../Charts/TempChart'; // Import the ECMLineChart component
import { HumidityGraph } from '../Charts/HumidChart'; // Import the ECMLineChart component
import { ButtonGroup, Button } from '@mui/material';
const MeasureDashboard = () => {
  const [selectedTimespan, setSelectedTimespan] = useState('1'); // Default to 1 hour
  const [averageECM, setAverageECM] = useState(0);
  const [averageTemp, setAverageTemp] = useState(0);
  const [averageHumid, setAverageHumid] = useState(0);
  const [chartData, setChartData] = useState([]);
  const [chartDataTemp, setChartDataTemp] = useState([]);
  const [chartDataHumid, setChartDataHumid] = useState([]);
  // Other state variables and useEffect for fetching data

  // Handle timespan selection
  const handleTimespanChange = (event: any) => {
    setSelectedTimespan(event.target.value);
  };

  // Fetch average ECM based on selected timespan
  useEffect(() => {
    fetchAverageECM();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedTimespan]);

  // Fetch average ECM datas
  const fetchAverageECM = async () => {
    try {
      const response = await axios.post(
        'https://i8agl9rmn4.execute-api.eu-north-1.amazonaws.com/dev/getAverageMeasures',
        {
          hours: selectedTimespan,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }
      );

      const averageECMValue: any = await calculateAverageECM(
        response.data.data
      );
      const averageTempValue: any = await calculateAverageTemperature(
        response.data.data
      );
      const averageHumidValue: any = await calculateAverageHumidity(
        response.data.data
      );

      setAverageECM(averageECMValue);
      setAverageTemp(averageTempValue);
      setAverageHumid(averageHumidValue);
      console.log('REESP: DATA : ', response.data.data);

      const chartData: any = response.data.data.map(
        (item: { Date: string; ECM: string }) => ({
          Date: item.Date,
          ECM: item.ECM,
        })
      );
      const chartDataTemp: any = response.data.data.map(
        (item: { Date: string; temperature: string }) => ({
          Date: item.Date,
          temperature: item.temperature,
        })
      );
      const chartDataHumid: any = response.data.data.map(
        (item: { Date: string; humidity: string }) => ({
          Date: item.Date,
          humidity: item.humidity,
        })
      );
      setChartData(chartData);
      setChartDataTemp(chartDataTemp);
      setChartDataHumid(chartDataHumid);
    } catch (error) {
      console.error('Error fetching average ECM:', error);
    }
  };

  // Calculate average ECM value from data array
  const calculateAverageECM = async (data: any) => {
    if (data.length === 0) return 0;
    if (Array.isArray(data)) {
      console.log(data.length);
    }
    let sum = 0;
    for (let i = 0; i < data.length; i++) {
      sum += parseInt(data[i].ECM);
    }
    let finalValue = sum / data.length;

    return finalValue.toFixed(2);
  };
  const calculateAverageTemperature = async (data: any) => {
    if (data.length === 0) return 0;
    if (Array.isArray(data)) {
      console.log(data.length);
    }
    let sum = 0;
    for (let i = 0; i < data.length; i++) {
      sum += parseInt(data[i].temperature);
    }
    let finalValue = sum / data.length;

    return finalValue.toFixed(2);
  };
  const calculateAverageHumidity = async (data: any) => {
    if (data.length === 0) return 0;
    if (Array.isArray(data)) {
      console.log(data.length);
    }
    let sum = 0;
    for (let i = 0; i < data.length; i++) {
      sum += parseInt(data[i].humidity);
    }
    let finalValue = sum / data.length;

    return finalValue.toFixed(2);
  };
  const buttons = [
    { value: '1', label: '1 Timmar' },
    { value: '6', label: '6 Timmar' },
    { value: '24', label: '24 Timmar' },
    { value: '168', label: '1 Vecka' },
  ];

  return (
    <div className="dashboard-container">
      <div className="average-container">
        <h1>Genomsnittligt EMC under vald period</h1>
        <p className="average-value">{averageECM}%</p>
      </div>

      <div className="select-container">
        {/* Select timespan */}
        <label className="select-label">Välj tidsperiod:</label>
        <div>
          <ButtonGroup
            color="primary"
            aria-label="medium secondary button group"
          >
            {buttons.map((button) => (
              <Button
                key={button.value}
                variant={
                  selectedTimespan === button.value ? 'contained' : 'outlined'
                }
                onClick={() =>
                  handleTimespanChange({ target: { value: button.value } })
                }
              >
                {button.label}
              </Button>
            ))}
          </ButtonGroup>
        </div>
      </div>
      {/* Display average ECM */}
      <div className="centered-container">
        <div className="graph-container">
          <h4> </h4>
          <SimpleGraph data={chartData} averageECM={averageECM} />
        </div>
      </div>
      {/* Display average Temp */}
      <h1>Graf över genomsnittlig temperatur</h1>
      <div className="centered-container">
        <div className="graph-container">
          <h4> </h4>
          <TemperatureGraph data={chartDataTemp} averageTemp={averageTemp} />
        </div>
      </div>
      {/* Display average Humidity */}
      <h1>Graf över genomsnittlig luftfuktighet</h1>
      <div className="centered-container">
        <div className="graph-container">
          <h4> </h4>
          <HumidityGraph data={chartDataHumid} averageHumid={averageHumid} />
        </div>
      </div>
      {/* Other components and UI elements */}
    </div>
  );
};

export default MeasureDashboard;

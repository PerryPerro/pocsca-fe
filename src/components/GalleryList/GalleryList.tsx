import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import '../../styles/ListStyling.css';
import { Link } from 'react-router-dom';
import AnalysPage from '../../pages/Analys/Analys';


const GalleryList = () => {
return (
  <ImageList
    cols={4}
    gap={0}
    style={{ padding: '0', marginTop: '0', overflow: 'hidden' }}
  >
    {/* Replace the image src with your actual image URLs */}
    {/* <Link to="/home"> */}
      <ImageListItem className="image-list-item">
        <img
          src="/images/scaStockFolder/scastock2.jpeg"
          alt="1"
          style={{ objectFit: 'cover', width: '100%', height: '40vh' }}
        />

      </ImageListItem>
    {/* </Link> */}
    {/* <Link to="/About"> */}
      <ImageListItem className="image-list-item">
        {' '}
        <img
          src="/images/scaStockFolder/scastock3.jpeg"
          alt="2"
          style={{ objectFit: 'cover', width: '100%', height: '40vh' }}
        />

      </ImageListItem>
    {/* </Link>
    <Link to="/AnalysPage"> */}
      <ImageListItem className="image-list-item">
        {' '}
        <img
          src="/images/scaStockFolder/scastock4.jpeg"
          alt="3"
          style={{ objectFit: 'cover', width: '100%', height: '40vh' }}
        />

      </ImageListItem>
    {/* </Link>
    <Link to="/ContactPage"> */}
      <ImageListItem className="image-list-item">
        {' '}
        <img
          src="/images/scaStockFolder/scastock5.jpeg"
          alt="4"
          style={{ objectFit: 'cover', width: '100%', height: '40vh' }}
        />

      </ImageListItem>
    {/* </Link> */}
  </ImageList>
);
}

export default GalleryList;
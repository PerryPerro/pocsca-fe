import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../../styles/DrySheetPage.css';
import DrySheetTable from '../../components/DrySheetTable/DrySheetTable'; // Import the DrySheetTable component

const DrySheetPage: React.FC = () => {
  const [batchData, setBatchData] = useState([]);

  const updateBatches = async () => {
    try {
      const response = await axios.get(
        'https://i8agl9rmn4.execute-api.eu-north-1.amazonaws.com/dev/getAllDryers'
      ); // Replace with your API endpoint

      setBatchData(response.data.data);
    } catch (error) {
      console.error('Error updating batches:', error);
    }
  };

  useEffect(() => {
    // Fetch data when the component mounts
    updateBatches();
  }, []);

  return (
    <div>
      <DrySheetTable batches={batchData} updateBatches={updateBatches} />
    </div>
  );
};

export default DrySheetPage;

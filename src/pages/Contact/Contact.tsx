import React from 'react';
import { Container, Typography, Grid, Card } from '@mui/material';
import { styled } from '@mui/system';
import ContactCard from '../../components/ContactCard/ContactCard';

const Section = styled('section')(({ theme }) => ({
  marginTop: theme.spacing(3),
  marginBottom: theme.spacing(3),
}));

const ContactPage: React.FC = () => {
  return (
    <Container>
      <Typography variant="h2" align="center" gutterBottom>
        About Us
      </Typography>
      <Section>
        <Typography variant="h4" gutterBottom>
          Our Mission
        </Typography>
        <Typography variant="body1">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
          accumsan blandit purus, a bibendum nunc vestibulum at. Aenean aliquet,
          ex sed tincidunt hendrerit, dolor ex luctus elit, et tincidunt purus
          massa eu nunc.
        </Typography>
      </Section>
      <Section>
        <Typography variant="h4" gutterBottom>
          Our Team
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12} md={4}>
            <ContactCard
              image="/images/avatarPlaceholder/placeholder3.png"
              name="John Doe"
              role="CEO"
              email="john@example.com"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <ContactCard
              image="/images/avatarPlaceholder/placeholder2.png"
              name="Jane Smith"
              role="CTO"
              email="jane@example.com"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <ContactCard
              image="/images/avatarPlaceholder/placeholder1.jpeg"
              name="Michael Johnson"
              role="Lead Developer"
              email="michael@example.com"
            />
          </Grid>
        </Grid>
      </Section>
    </Container>
  );
};

export default ContactPage;

// AnalysPage.js
import React from 'react';
import MeasureList from '../../components/MeasureList/Measurelist'
import '../../styles/AnalysPage.css'

const AnalysPage: React.FC = () => {
  return (
    <div className="grid-container">
      <div className="grid-item measures">
        <h2>Measures List</h2>
        <MeasureList />
      </div>
    </div>
  );
};

export default AnalysPage;

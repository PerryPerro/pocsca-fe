import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

interface ChartDataItem {
  Date: string; // Adjust the type based on your actual data structure
  temperature: string; // Adjust the type based on your actual data structure
}

interface TemperatureGraphProps {
  data: ChartDataItem[];
  averageTemp: number;
}

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const TemperatureGraph: React.FC<TemperatureGraphProps> = ({
  data,
  averageTemp,
}) => {
  const timeLabels = data.map((item) => {
    let fixed = item.Date.split('T')[1];
    let removedChar = fixed.lastIndexOf(':');
    let finalFormat = fixed.substring(0, removedChar);
    return finalFormat;
  });

  const temperatureValues = data.map((item) => item.temperature);
  const chartData = {
    labels: timeLabels,
    datasets: [
      {
        label: 'Temperatur mätningar', // Change the label
        data: temperatureValues,
        borderColor: 'rgb(255, 159, 64)', // Customize the color here (orange)
        backgroundColor: 'rgba(255, 159, 64, 0.5)', // Customize the color here (orange with opacity)
      },
      {
        label: 'Genomsnittlig Temperatur', // Change the label
        data: Array(temperatureValues.length).fill(averageTemp),
        borderColor: 'rgb(75, 192, 192)', // Customize the color here (teal)
        backgroundColor: 'rgba(75, 192, 192, 0.5)', // Customize the color here (teal with opacity)
      },
    ],
  };

  const options = {
    responsive: true,

    plugins: {
      legend: {
        position: 'top' as const,
      },
      scales: {
        y: {
          beginAtZero: true,
          title: {
            display: true,
            text: 'Value',
          },
        },
      },
    },
  };

  return <Line options={options} data={chartData} />;
};

import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

interface ChartDataItem {
  Date: string; // Adjust the type based on your actual data structure
  humidity: string; // Adjust the type based on your actual data structure
}

interface HumidityGraphProps {
  data: ChartDataItem[];
  averageHumid: number;
}

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const HumidityGraph: React.FC<HumidityGraphProps> = ({
  data,
  averageHumid,
}) => {
  const timeLabels = data.map((item) => {
    let fixed = item.Date.split('T')[1];
    let removedChar = fixed.lastIndexOf(':');
    let finalFormat = fixed.substring(0, removedChar);
    return finalFormat;
  });

  const humidityValues = data.map((item) => item.humidity);
  const chartData = {
    labels: timeLabels,
    datasets: [
      {
        label: 'Luftfuktighets mätningar',
        data: humidityValues,
        borderColor: 'rgb(0, 128, 0)', // Customize the color here (green)
        backgroundColor: 'rgba(0, 128, 0, 0.5)', // Customize the color here (green with opacity)
      },
      {
        label: 'Genomsnittligt EMC',
        data: Array(humidityValues.length).fill(averageHumid),
        borderColor: 'rgb(0, 0, 255)', // Customize the color here (blue)
        backgroundColor: 'rgba(0, 0, 255, 0.2)', // Customize the color here (blue with opacity)
      },
    ],
  };

  const options = {
    responsive: true,

    plugins: {
      legend: {
        position: 'top' as const,
      },
      scales: {
        y: {
          beginAtZero: true,
          title: {
            display: true,
            text: 'Value',
          },
        },
      },
    },
  };

  return <Line options={options} data={chartData} />;
};

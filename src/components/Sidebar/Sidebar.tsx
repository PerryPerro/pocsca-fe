import React from 'react';
import {
  List,
  ListItemIcon,
  ListItemText,
  ListItemButton,
} from '@mui/material';
import { ShowChart, Search, AddBox } from '@mui/icons-material';
import  '../../styles/Sidebar.css';
const buttonStyling = {
  border: '2px solid #FFFFFF',
  backgroundColor: '#D3D3D3 ',
  marginBottom: '10px',
  fontWeight: 'light',
};

interface SidebarProps {
  setActiveSection: (section: string) => void;
}

const Sidebar: React.FC<SidebarProps> = ({ setActiveSection }) => {
  const handleSectionClick = (section: string) => {
    setActiveSection(section);
  };

  return (
    <div className="sidebar">
      <List className="horizontal-list">
        <ListItemButton
          // Add the class name
          onClick={() => handleSectionClick('show-batches')}
          sx={buttonStyling}
        >
          <ListItemIcon>
            <ShowChart />
          </ListItemIcon>
          <ListItemText primary="Show Batches" />
        </ListItemButton>
        <ListItemButton
          // Add the class name
          onClick={() => handleSectionClick('find-batch')}
          sx={buttonStyling}
        >
          <ListItemIcon>
            <Search />
          </ListItemIcon>
          {/* <ListItemText primary="Find Batch" /> */}
          <ListItemText>
            <span style={{ fontSize: '1.5rem' }}>Find Batch</span>
          </ListItemText>
        </ListItemButton>
        <ListItemButton
          // Add the class name
          onClick={() => handleSectionClick('create-batch')}
          sx={buttonStyling}
        >
          <ListItemIcon>
            <AddBox />
          </ListItemIcon>
          <ListItemText primary="Create Batch" />
        </ListItemButton>
      </List>
    </div>
  );
};

export default Sidebar;

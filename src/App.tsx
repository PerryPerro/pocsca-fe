import React from 'react';
import Home from './pages/Home';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import AnalysPage from './pages/Analys/Analys';
import About from './pages/About/About';
import DrySheetPage from './pages/DrySheet/DrySheetPage';
import ContactPage from './pages/Contact/Contact'
import Layout from './components/Applayout/Layout';



function App() {
  return (
    <div>
      <Router>
        <Layout>
          <Routes>
            {/* <Route path="/about" element={About} /> */}
            <Route path="/ContactPage" element={<ContactPage />} />
            <Route path="/AnalysPage" element={<AnalysPage />} />{' '}
            <Route path="/home" element={<Home />} />
            <Route path="/About" element={<About />} />
            <Route path="/DrySheetPage" element={<DrySheetPage />} />
            {/* Use the AnalysPage component */}
          </Routes>
        </Layout>
      </Router>
    </div>
  );
}

export default App;

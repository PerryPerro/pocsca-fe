import React, { useState } from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import PersonIcon from '@mui/icons-material/Person';
import HomeIcon from '@mui/icons-material/Home';
import TimelineIcon from '@mui/icons-material/Timeline';
import BatchPredictionIcon from '@mui/icons-material/BatchPrediction';
import ContactsIcon from '@mui/icons-material/Contacts';
import '../../styles/ListStyling.css';
import GalleryList from '../GalleryList/GalleryList';
import { Link, NavLink } from 'react-router-dom';
import WindowIcon from '@mui/icons-material/Window';
import AnalyticsOutlinedIcon from '@mui/icons-material/AnalyticsOutlined';
import ApartmentOutlinedIcon from '@mui/icons-material/ApartmentOutlined';

interface LayoutProps {
  children: React.ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const toggleDrawer =
    (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }

      setDrawerOpen(open);
    };
  return (
    <div
      style={{ minHeight: '600vh', display: 'flex', flexDirection: 'column' }}
    >
      <AppBar position="static" style={{ height: 'auto' }}>
        <Toolbar
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Box display="flex" alignItems="center">
            <IconButton
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={toggleDrawer(true)}
            >
              <MenuIcon style={{height: '40px', width: '40px'}} />
            </IconButton>
            <img
              src="/images/scalogo.png"
              alt="Logo"
              style={{
                width: '100px',
                height: '100px',
                marginRight: '20px',
                marginBottom: '5px',
              }}
            />
            <NavLink
              to="/home"
              className={({ isActive }) =>
                `link-button${isActive ? ' active-link' : ''}`
              }
            >
              <Typography
                variant="h4"
                style={{
                  fontSize: '45px',
                  fontWeight: 'bold',
                  marginBottom: '-10px',
                  marginRight: '40px',
                }}
              >
                Mätarstationen
              </Typography>
            </NavLink>

            <Box display="flex" style={{ marginTop: '20px' }}>
              <NavLink
                to="/AnalysPage"
                className={({ isActive }) =>
                  `link-button${isActive ? ' active-link' : ''}`
                }
              >
                Analys
              </NavLink>
              <NavLink
                to="/About"
                className={({ isActive }) =>
                  `link-button${isActive ? ' active-link' : ''}`
                }
              >
                Batches
              </NavLink>
              <NavLink
                to="/ContactPage"
                className={({ isActive }) =>
                  `link-button${isActive ? ' active-link' : ''}`
                }
              >
                Contact
              </NavLink>
            </Box>
          </Box>
        </Toolbar>
      </AppBar>
      <Drawer anchor="left" open={drawerOpen} onClose={toggleDrawer(false)}>
        <List style={{ width: '250px' }}>
          {/* Additional items in the side menu */}
          <ListItemButton component={Link} to="/profile">
            <PersonIcon style={{ marginRight: '10px' }} />
            <ListItemText
              primaryTypographyProps={{ fontSize: '30px' }}
              primary="Profile"
            />
          </ListItemButton>
          <ListItemButton component={Link} to="/DrySheetPage">
            <WindowIcon style={{ marginRight: '10px' }} />{' '}
            {/* Replace with your icon */}
            <ListItemText
              primaryTypographyProps={{ fontSize: '30px' }}
              primary="Torktavlan"
            />
          </ListItemButton>
          <ListItemButton component={Link} to="/extraPage2">
            <ApartmentOutlinedIcon style={{ marginRight: '10px' }} />{' '}
            {/* Replace with your icon */}
            <ListItemText
              primaryTypographyProps={{ fontSize: '30px' }}
              primary="Mätstationen"
            />
          </ListItemButton>
          {/* ... Add more items as needed */}
        </List>
      </Drawer>
      <GalleryList />

      <Container style={{ flexGrow: 1 }}>{children}</Container>
    </div>
  );
};

export default Layout;

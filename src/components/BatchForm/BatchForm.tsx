import React, { useState, useEffect } from 'react';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { SelectChangeEvent } from '@mui/material/Select';
interface BatchFormProps {
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (modifiedData: any) => void; // Replace 'any' with the correct type for modified data
  batch: any; // Replace 'Batch' with the correct type for batch data
}

const BatchForm: React.FC<BatchFormProps> = ({
  isOpen,
  onClose,
  onSubmit,
  batch,
}) => {
  const initialFormState = {
    dryerId: '',
    startDate: '',
    status: '',
    information: '',
    dryingTime: '',
    programTime: '',
    mc: '',
    dimension: '',
    endDate: '',
    dryingProgram: '',
  };
const getCurrentTimeInSweden = () => {
  const now = new Date();
  const swedenTimezoneOffset = 2 * 60; // Sweden is UTC+2
  const currentTime = new Date(now.getTime() + swedenTimezoneOffset * 60000);

  const year = currentTime.getUTCFullYear();
  const month = `0${currentTime.getUTCMonth() + 1}`.slice(-2); // Add 1 because months are zero-based
  const day = `0${currentTime.getUTCDate()}`.slice(-2);
  const hours = `0${currentTime.getUTCHours()}`.slice(-2);
  const minutes = `0${currentTime.getUTCMinutes()}`.slice(-2);

  const formattedDateTime = `${year}-${month}-${day}T${hours}:${minutes}`;
  return formattedDateTime;
};
  const [formData, setFormData] = useState(initialFormState);

  React.useEffect(() => {
    if (batch) {
      setFormData({
        ...batch,
      });
    }
  }, [batch]);


const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
  const { name, value } = event.target;
  setFormData((prevData) => ({
    ...prevData,
    [name]: value,
  }));
};
  const handleInputChangeSelect = (
    event: SelectChangeEvent<string>,
    child: React.ReactNode
  ) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };


  const handleSubmit = () => {
    // Implement logic to submit the batch update to the API
    onSubmit(formData);
  };

  return (
    <Dialog open={isOpen} onClose={onClose}>
      <DialogTitle>Edit Batch</DialogTitle>
      <DialogContent>
        <TextField
          label="Dryer ID"
          name="dryerId"
          value={formData.dryerId || ''}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Start Date"
          name="startDate"
          value={formData.startDate || getCurrentTimeInSweden()}
          onChange={handleInputChange}
          fullWidth
          margin="dense"
        />
        <Select
          label="Status"
          name="status"
          value={formData.status || 'Tom'} // Set the default value to 'Tom'
          onChange={handleInputChangeSelect}
          fullWidth
        >
          <MenuItem value="Tom">Tom</MenuItem>
          <MenuItem value="Pågående">Pågående</MenuItem>
          <MenuItem value="Redo att tömmas">Redo att tömmas</MenuItem>
        </Select>
        <TextField
          label="Information"
          name="information"
          value={formData.information || ''}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Drying Time"
          name="dryingTime"
          value={formData.dryingTime || ''}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Program Time"
          name="programTime"
          value={formData.programTime || ''}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="MC"
          name="mc"
          value={formData.mc || ''}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Dimension"
          name="dimension"
          value={formData.dimension || ''}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="End Date"
          name="endDate"
          value={formData.endDate || ''}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Drying Program"
          name="dryingProgram"
          value={formData.dryingProgram || ''}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <Button variant="contained" color="primary" onClick={handleSubmit}>
          Save
        </Button>
        <Button variant="contained" color="secondary" onClick={onClose}>
          Cancel
        </Button>
      </DialogContent>
    </Dialog>
  );
};

export default BatchForm;

import React from 'react';
import Grid from '@mui/material/Grid';
import DataDisplay from '../components/DataDisplay/Datadisplay';
import AverageDataDashboard from '../components/AverageDataDashboard/AverageDataDashboard';

const Home: React.FC = () => {
  const headingStyles: React.CSSProperties = {
    fontFamily: 'Helvetica, sans-serif',
    fontSize: '4rem',
    textAlign: 'center',
    marginTop: '2rem',
    color: 'grey',
  };

  return (
    <div>
      <Grid container spacing={3}>

        <Grid item xs={12} md={12} order={1}>
          <div style={{ textAlign: 'center' }}>
            <DataDisplay />
          </div>
        </Grid>
        <Grid item xs={4} order={2} md={12}>
          <AverageDataDashboard />
        </Grid>
      </Grid>

    </div>
  );

};

export default Home;

import React, { useState } from 'react';
import Sidebar from '../../components/Sidebar/Sidebar';
import ShowBatches from '../../components/BatchComponents/ShowBatches';
import CreateBatch from '../../components/BatchComponents/CreateBatch';
import FindBatch from '../../components/BatchComponents/FindBatch';

const About: React.FC = () => {
  const [activeSection, setActiveSection] = useState('show-batches'); // Default active section

  const renderSection = () => {
    switch (activeSection) {
      case 'show-batches':
        return <ShowBatches />;
      case 'create-batch':
        return <CreateBatch />;
      case 'find-batch':
        return <FindBatch />;
      default:
        return null;
    }
  };

  return (
    <div>
      <Sidebar setActiveSection={setActiveSection} />
      <div className="main-content">{renderSection()}</div>
    </div>
  );
};

export default About;

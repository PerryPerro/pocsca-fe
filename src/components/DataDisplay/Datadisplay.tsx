import React, { useEffect, useState } from 'react';
import axios from 'axios';

const celsiusToFahrenheit = (celsius: any) => {
  let fahrenheit = (celsius * 9) / 5 + 32;
  return fahrenheit;
};

const calculateEMC = (fahrenheit: any, humidity: any) => {
  const T = fahrenheit;
  const H = humidity / 100;
  console.log('HHHH', H);
  const W = 330 + 0.452 * T + 0.00415 * Math.pow(T, 2);
  const K = 0.791 + 0.000463 * T - 0.000000844 * Math.pow(T, 2);
  const K1 = 6.34 + 0.000775 * T - 0.0000935 * Math.pow(T, 2);
  const K2 = 1.09 + 0.0284 * T - 0.0000904 * Math.pow(T, 2);

  const numerator =
    (K * H) / (1 - K * H) +
    (K1 * H + 2 * K1 * K2 * Math.pow(H, 2)) /
      (1 + K1 * H + K1 * K2 * Math.pow(H, 2));
  const denominator = 1800 / W;
  const M = denominator * numerator;

  return M; // Rounding the result to 2 decimal places
};
const DataDisplay = () => {
  const [data, setData] = useState<any>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const humidity = await axios.get(
          'https://opendata-download-metobs.smhi.se/api/version/1.0/parameter/6/station/127310/period/latest-hour/data.json'
        );
        const temperature = await axios.get(
          'https://opendata-download-metobs.smhi.se/api/version/1.0/parameter/1/station/127310/period/latest-hour/data.json'
        );
        let toFahrenheit = celsiusToFahrenheit(temperature.data.value[0].value);
        let calcEMC = calculateEMC(
          toFahrenheit,
          humidity.data.value[0].value
        ).toFixed(2);

        // Assuming both responses contain arrays of data, you can concatenate them into a single array
        const combinedData = {
          Humidity: humidity.data.value[0].value,
          Temperature: temperature.data.value[0].value,
          EMC: calcEMC,
        };

        setData(combinedData);
        console.log(combinedData);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);
  const containerStyles: React.CSSProperties = {
    marginTop: '20px'// Add a solid border with a color that complements the text color
  };

  const headingStyles: React.CSSProperties = {
    fontFamily: 'Helvetica, sans-serif',
    fontSize: '1.5rem',
    color: 'black', // Match the border color
    marginTop: '0',
    marginBottom: '0.5rem', // Add some space between headings
  };

  return (
    <div style={containerStyles}>
      <div style={{ backgroundColor: '#1e6ab7' }}>
        <h1
          style={{
            fontSize: '3rem',
            marginTop: 0,
            marginBottom: '1rem',
            color: 'white',
          }}
        >
          Mätstationsvärden
        </h1>
      </div>
      <div >
        <h1 style={headingStyles}>Luftfuktighet: {data.Humidity}%</h1>
        <h1 style={headingStyles}>Temperatur: {data.Temperature} C</h1>
        <h1 style={headingStyles}>Jämnviktsfuktkvot: {data.EMC}%</h1>
      </div>
    </div>
  );
};

export default DataDisplay;

import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {
  List,
  ListItem,
  ListItemText,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  TextField,
  FormControlLabel,
  Checkbox,
} from '@mui/material';
interface SearchOptions {
  batchId: boolean;
  batchMaterial: boolean;
  batchDimension: boolean;
  [key: string]: boolean; // Add an index signature
}

const ShowBatches: React.FC = () => {
  const [batches, setBatches] = useState([]);
  const [selectedBatch, setSelectedBatch] = useState(null);
  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [searchQuery, setSearchQuery] = useState(''); // State for search input
  const [searchOptions, setSearchOptions] = useState<SearchOptions>({
    batchId: true,
    batchMaterial: true,
    batchDimension: true,
  });

  useEffect(() => {
    axios
      .get(
        'https://i8agl9rmn4.execute-api.eu-north-1.amazonaws.com/dev/getAllBatches'
      )
      .then((response) => {
        setBatches(response.data.data);
      })
      .catch((error) => {
        console.error('Error fetching batches:', error);
      });
  }, []);

  const handleBatchClick = (batch: any) => {
    setSelectedBatch(batch);
    setIsPopupOpen(true);
  };

  const handleClosePopup = () => {
    setIsPopupOpen(false);
  };

  const filteredBatches = batches.filter((batch: any) => {
    const searchLower = searchQuery.toLowerCase();

    if (
      Object.keys(searchOptions).every((key) => !searchOptions[key]) ||
      !searchQuery
    ) {
      // Display all items when no checkboxes are checked or searchQuery is empty
      return true;
    }

    return Object.keys(searchOptions).some((key) => {
      if (!searchOptions[key]) {
        return false;
      }

      if (!batch[key]) {
        return false;
      }

      return batch[key].toLowerCase().includes(searchLower);
    });
  });

  const handleSearchOptionChange = (option: string) => {
    setSearchOptions((prevOptions) => ({
      ...prevOptions,
      [option]: !prevOptions[option],
    }));
  };

  return (
    <div>
      <h2>Show Batches</h2>
      {/* Search Input */}
      <TextField
        label="Search"
        variant="outlined"
        fullWidth
        value={searchQuery}
        onChange={(e) => setSearchQuery(e.target.value)}
        style={{ marginBottom: '20px' }}
      />
      {/* Search Options Checkboxes */}
      {Object.keys(searchOptions).map((option) => (
        <FormControlLabel
          key={option}
          control={
            <Checkbox
              checked={searchOptions[option]}
              onChange={() => handleSearchOptionChange(option)}
            />
          }
          label={option.toUpperCase()}
        />
      ))}
      <List>
        {filteredBatches.map((batch: any, index) => (
          <ListItem
            key={index}
            onClick={() => handleBatchClick(batch)}
            sx={{
              border: '1px solid #ccc',
              cursor: 'pointer',
              transition: 'background-color 0.3s',
              '&:hover': {
                backgroundColor: '#f5f5f5',
              },
            }}
          >
            <ListItemText
              primary={`Batch ID: ${batch.batchId}`}
              secondary={
                <React.Fragment>
                  <Typography
                    component="span"
                    variant="body2"
                    color="textPrimary"
                  >
                    Material: {batch.batchMaterial}
                  </Typography>
                  <br />
                  Dimension: {batch.batchDimension}
                  <br />
                  Measured EMC: {batch.MeasuredEMC}
                  <br />
                  Estimated EMC: {batch.EstimatedEMC}
                  <br />
                  Batch Size: {batch.batchSize}
                  <br />
                  Valid EMC Range: {batch.validEMCRange}
                  <br />
                  Created Date: {batch.createdDate}
                  <br />
                  Estimated Finish: {batch.estimatedFinish}
                </React.Fragment>
              }
            />
          </ListItem>
        ))}
      </List>

      {/* Batch Info Popup */}
      <Dialog open={isPopupOpen} onClose={handleClosePopup}>
        <DialogTitle>Batch Information</DialogTitle>
        {selectedBatch && (
          <DialogContent>
            {/* Display batch information here */}
            {/* You can format it similar to how it's shown in the list */}
          </DialogContent>
        )}
        <DialogActions>
          <Button onClick={handleClosePopup}>Close</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ShowBatches;

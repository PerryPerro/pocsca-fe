import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

interface ChartDataItem {
  Date: string; // Adjust the type based on your actual data structure
  ECM: string; // Adjust the type based on your actual data structure
}

interface SimpleGraphProps {
  data: ChartDataItem[];
  averageECM: number;
}

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const SimpleGraph: React.FC<SimpleGraphProps> = ({
  data,
  averageECM,
}) => {


  const timeLabels = data.map((item) => {
    let fixed = item.Date.split('T')[1];
    let removedChar = fixed.lastIndexOf(':');
    let finalFormat = fixed.substring(0, removedChar);
    return finalFormat;
  });

  const ecmValues = data.map((item) => item.ECM);
  const chartData = {
    labels: timeLabels,
    datasets: [
      {
        label: 'EMC mätningar',
        data: ecmValues,
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
      {
        label: 'Genomsnittligt EMC',
        data: Array(ecmValues.length).fill(averageECM),
        borderColor: 'rgb(60, 20, 255)', // Light blue color
      },
    ],
  };

  const options = {
    responsive: true,

    plugins: {
      legend: {
        position: 'top' as const,
      },
      scales: {
        y: {
          beginAtZero: true,
          title: {
            display: true,
            text: 'Value',
          },
        },
      },

    },
  };

  return <Line options={options} data={chartData} />;
};

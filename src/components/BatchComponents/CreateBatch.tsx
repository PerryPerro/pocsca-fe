import React, { useState } from 'react';
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import axios from 'axios';
import { SelectChangeEvent } from '@mui/material';

type Dimension = {
  dimension: string;
  label: string;
};

const materialToDimensions: any = {
  Gran: [
    { dimension: '16', label: '16mm' },
    { dimension: '22', label: '22mm' },
    { dimension: '28', label: '28mm' },
    { dimension: '30', label: '30mm' },
    { dimension: '38', label: '38mm' },
    { dimension: '40', label: '40mm' },
    { dimension: '43', label: '43mm' },
    { dimension: '44', label: '44mm' },
    { dimension: '47', label: '47mm' },
    { dimension: '50', label: '50mm' },
    { dimension: '60', label: '60mm' },
    { dimension: '63', label: '63mm' },
  ],
  Tall: [
    { dimension: 'dimension3', label: 'Dimension 3' },
    { dimension: 'dimension4', label: 'Dimension 4' },
  ],
  // Add more materials and their dimensions as needed
};
const CreateBatch: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [batchData, setBatchData] = useState({
    batchId: '',
    batchMaterial: '',
    batchDimension: '',
    measuredEMC: '',
    estimatedEMC: '',
    batchSize: '',
    validEMCRange: '' as string | string[],
    createdDate: '',
    estimatedFinish: '',
  });
  const [successDialogOpen, setSuccessDialogOpen] = useState(false);
  const [DialogMessage, setDialogMessage] = useState();
  const [errorDialogOpen, setErrorDialogOpen] = useState(false);
  const [selectedValidEMC, setSelectedValidEMC] = useState<string>(''); // Initialize with an empty string
  const [selectedValidEMCOptions, setSelectedValidEMCOptions] = useState<
    string[]
  >([]);

  const handleMaterialChange = (
    e: SelectChangeEvent<string>,
    child: React.ReactNode
  ) => {
    const selectedMaterial = e.target.value;
    setBatchData({
      ...batchData,
      batchMaterial: selectedMaterial,
      batchDimension: '',
      validEMCRange: '', // Reset validEMCRange when material changes
    });
  };

  const handleDimensionChange = (
    e: SelectChangeEvent<string>,
    child: React.ReactNode
  ) => {
    const selectedDimension = e.target.value;
    const validEMCOptions = getValidEMCRangeForDimension(
      batchData.batchMaterial,
      selectedDimension
    );

    // Derive the selected valid EMC options for the current dimension
    // const selectedValidEMCOptions = validEMCRange;

    setBatchData({
      ...batchData,
      batchDimension: selectedDimension,
      validEMCRange: validEMCOptions,
    });

    // Reset the selected valid EMC option and set the valid EMC options
    setSelectedValidEMC('');
    setSelectedValidEMCOptions(validEMCOptions);
  };

  const calculateEMCOptionsForDimension = (dimensionInfo: any): string[] => {
    // Implement your logic here to calculate the valid EMC range options
    // based on the selected dimension
    // For example:
    console.log('DIMENSION: ', dimensionInfo.dimension);
    switch(dimensionInfo.dimension){
      case('16'): return ['14', '16']
      case('22'): return ['18']
      case('28'): return ['18']
      case('30'): return ['12']
      case('38'): return ['11', '13', '18']
      case('43'): return ['12']
      case('44'): return ['12', '18']
      case('47'): return ['12', '18']
      case('50'): return ['12', '16', '18']
      case('60'): return ['18']
      case('63'): return ['18']
    }
    // Replace this with your actual logic
    if (dimensionInfo.dimension === '16') {
      console.log('INSIDE IF');

      return ['18'];
    } else if (dimensionInfo.dimension === '24') {
      return ['14', '16'];
    } else {
      return []; // Default empty array
    }
  };

  const getValidEMCRangeForDimension = (
    material: string,
    dimension: string
  ): string[] => {
    const dimensionInfo = materialToDimensions[material].find(
      (item: any) => item.dimension === dimension
    );

    if (dimensionInfo) {
      // Calculate the valid EMC range options based on the selected dimension
      const validEMCOptions = calculateEMCOptionsForDimension(dimensionInfo);
      return validEMCOptions;
    }

    return [];
  };

const handleSubmit = async () => {
  try {
    const response = await axios.post(
      'https://i8agl9rmn4.execute-api.eu-north-1.amazonaws.com/dev/createBatch',
      {
        ...batchData,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
        },
      }
    );
      console.log(response.data.message);

    // Check the response and show the appropriate popup
    if (response.status === 200) {
      setDialogMessage(response.data.message);
      setSuccessDialogOpen(true);

    } else {
      setDialogMessage(response.data.message);
      setErrorDialogOpen(true);
    }
  } catch (error) {
    setErrorDialogOpen(true);
  }

  // Close the dialog after submitting
  handleClose();
};

  const handleOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <div>
      <h2>Create Batch</h2>
      <Button variant="contained" onClick={handleOpen}>
        Skapa ny batch
      </Button>

      <Dialog open={isOpen} onClose={handleClose}>
        <DialogTitle>Lägg till information för batch</DialogTitle>
        <DialogContent>
          <TextField
            label="Batch Id"
            value={batchData.batchId}
            onChange={(e) =>
              setBatchData({ ...batchData, batchId: e.target.value })
            }
            fullWidth
            margin="dense"
          />
          <FormControl fullWidth margin="dense">
            <InputLabel
              style={{ fontWeight: 'bold', fontSize: '18px', color: 'Black' }}
            >
              Batch Material
            </InputLabel>
            <Select
              value={batchData.batchMaterial}
              onChange={handleMaterialChange}
            >
              <MenuItem value="Gran">Gran</MenuItem>
              <MenuItem value="Tall">Tall</MenuItem>
              <MenuItem value="Björk">Björk</MenuItem>
              {/* Add more menu items as needed */}
            </Select>
          </FormControl>
          <FormControl fullWidth margin="dense">
            <InputLabel
              style={{ fontWeight: 'bold', fontSize: '18px', color: 'Black' }}
            >
              Batch Dimension
            </InputLabel>
            <Select
              value={batchData.batchDimension}
              onChange={handleDimensionChange}
              disabled={!batchData.batchMaterial}
            >
              {batchData.batchMaterial &&
                materialToDimensions[
                  batchData.batchMaterial as keyof typeof materialToDimensions
                ].map((dimensionInfo: Dimension) => (
                  <MenuItem
                    key={dimensionInfo.dimension}
                    value={dimensionInfo.dimension}
                  >
                    {dimensionInfo.label}
                  </MenuItem>
                ))}
            </Select>
          </FormControl>
          <FormControl fullWidth margin="dense">
            <InputLabel
              style={{ fontWeight: 'bold', fontSize: '18px', color: 'Black' }}
            >
              Valid EMC Range
            </InputLabel>
            <Select
              value={selectedValidEMC}
              onChange={(e) => setSelectedValidEMC(e.target.value as string)}
            >
              {/* Map over the valid EMC options and create menu items */}
              {selectedValidEMCOptions.map((emcOption) => (
                <MenuItem key={emcOption} value={emcOption}>
                  {emcOption}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          <TextField
            label="Measured EMC"
            value={batchData.measuredEMC}
            onChange={(e) =>
              setBatchData({ ...batchData, measuredEMC: e.target.value })
            }
            fullWidth
            margin="dense"
          />
          <TextField
            label="Batch Size... in kg"
            value={batchData.batchSize}
            onChange={(e) =>
              setBatchData({ ...batchData, batchSize: e.target.value })
            }
            fullWidth
            margin="dense"
          />

          {/* Add other TextField components for other input fields */}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSubmit}>Create</Button>
        </DialogActions>
      </Dialog>
      {/* Success Dialog */}
      <Dialog
        open={successDialogOpen}
        onClose={() => setSuccessDialogOpen(false)}
      >
        <DialogTitle>Success</DialogTitle>
        <DialogContent>{DialogMessage}</DialogContent>
        <DialogActions>
          <Button onClick={() => setSuccessDialogOpen(false)}>OK</Button>
        </DialogActions>
      </Dialog>

      {/* Error Dialog */}
      <Dialog open={errorDialogOpen} onClose={() => setErrorDialogOpen(false)}>
        <DialogTitle>Error</DialogTitle>
        <DialogContent>{DialogMessage}</DialogContent>
        <DialogActions>
          <Button onClick={() => setErrorDialogOpen(false)}>OK</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default CreateBatch;

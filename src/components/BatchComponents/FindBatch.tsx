import React, { useState } from 'react';
import { Button, TextField } from '@mui/material';
import axios from 'axios';

const FindBatch: React.FC = () => {
  const [batchId, setBatchId] = useState('');
  const [batchData, setBatchData]: any = useState(null);

  const handleFind = () => {
    axios
      .get(`getBatchById/${batchId}`)
      .then((response) => {
        console.log('Batch found:', response.data);
        setBatchData(response.data);
      })
      .catch((error) => {
        console.error('Error finding batch:', error);
      });
  };

  return (
    <div>
      <h2>Find Batch</h2>
      <TextField
        label="Batch ID"
        value={batchId}
        onChange={(e) => setBatchId(e.target.value)}
      />
      <Button variant="contained" onClick={handleFind}>
        Find
      </Button>
      {batchData && (
        <div>
          <h3>Batch Details</h3>
          <p>ID: {batchData.id}</p>
          <p>Name: {batchData.name}</p>
          {/* Add more details here */}
        </div>
      )}
    </div>
  );
};

export default FindBatch;

import React, { useEffect, useState } from 'react';
import axios from 'axios';
import '../../styles/MeasureList.css';

const MeasureList = () => {
  const [measures, setMeasures] = useState<any[]>([]);
  const [itemsToShow, setItemsToShow] = useState<number>(10); // Number of items to show initially

  useEffect(() => {
    fetchMeasures();
  }, []);

  const fetchMeasures = async () => {
    try {
      const response = await axios.get(
        'https://i8agl9rmn4.execute-api.eu-north-1.amazonaws.com/dev/getMeasures',
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      console.log('FETCHED DATA', response);

      setMeasures(response.data.data);
    } catch (error) {
      console.error('Error fetching measures:', error);
    }
  };

  const headerStyles: React.CSSProperties = {
    textAlign: 'center',
  };

  // const handleLoadMore = () => {
  //   setItemsToShow(itemsToShow + 10); // Increase the number of items to show
  // };

  return (
    <div className="measure-list-container">
      <h2 style={headerStyles}>Tidigare Mätningar</h2>
      {measures?.slice(0, itemsToShow).map((measure: any, index: number)   => (
        <div className="measure-item" key={index}>
          <p>Timestamp: {measure.Date}</p>
          <p>Temperature: {measure.temperature}</p>
          <p>Humidity: {measure.humidity}</p>
          <p>EMC: {measure.ECM}</p>
          <hr />
        </div>
      ))}

      <div className="load-more-container">
        {itemsToShow < measures.length && (
          <button
            className="load-more-button"
            onClick={() => setItemsToShow(itemsToShow + 10)}
          >
            Load More
          </button>
        )}
      </div>
    </div>
  );
};

export default MeasureList;

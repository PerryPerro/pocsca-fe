import React, { useState } from 'react';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import Button from '@mui/material/Button';
import CloseIcon from '@mui/icons-material/Close';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import SettingsIcon from '@mui/icons-material/Settings';
import '../../styles/DrySheetTable.css';
import axios from 'axios';
import BatchForm from '../../components/BatchForm/BatchForm'; // Import the BatchForm component

import AppsIcon from '@mui/icons-material/Apps';
import AspectRatioIcon from '@mui/icons-material/AspectRatio';

interface Batch {
  dryerId: string;
  startDate: string;
  status: string;
  information: string;
  dryingTime: string;
  programTime: string;
  mc: string;
  dimension: string;
  endDate: string;
  dryingProgram: string;
}

// Define an object to map status values to CSS classes
const statusToStyle: Record<string, React.CSSProperties> = {
  Tom: {
    backgroundColor: 'lightblue',
  },
  Klar: {
    backgroundColor: 'lightyellow',
  },
  Pågående: {
    backgroundColor: 'lightGreen',
  },
  Reparation: {
    backgroundColor: 'red',
  },
};
const StatusToStyleMap: Record<string, React.CSSProperties> = {
  Tom: {
    backgroundColor: '#3c7ad6', // Change color as needed
  },
  Klar: {
    backgroundColor: '#a3b02e', // Change color as needed
  },
  Pågående: {
    backgroundColor: '#32a852', // Change color as needed
  },
  Reparation: {
    backgroundColor: '#a61140', // Change color as needed
  },
};

interface DrySheetTableProps {
  batches: Batch[];
  updateBatches: () => Promise<void>;
}

const DrySheetTable: React.FC<DrySheetTableProps> = ({
  batches,
  updateBatches,
}) => {
  const [isGalleryView, setGalleryView] = useState(false);
  const [selectedBatch, setSelectedBatch] = useState<Batch | null>(null);
  const [isDialogOpen, setDialogOpen] = useState(false);
  const [isBatchFormOpen, setBatchFormOpen] = useState(false); // State for BatchForm
  const [isConfirmationPromptOpen, setConfirmationPromptOpen] = useState(false); // State for confirmation prompt


  const toggleView = (gallery: boolean) => {
    setGalleryView(gallery);
  };

  const handleBatchClick = (batch: Batch) => {
    setSelectedBatch(batch);
    setDialogOpen(true);
  };

  const handleCloseDialog = () => {
    setSelectedBatch(null);
    setDialogOpen(false);
  };

  const handleStartTorkingClick = () => {
    setBatchFormOpen(true); // Open the BatchForm
  };

  const handleCloseBatchForm = () => {
    setBatchFormOpen(false); // Close the BatchForm
  };

  const openConfirmationPrompt = () => {
    setConfirmationPromptOpen(true);
  };

const closeConfirmationPrompt = () => {
  setConfirmationPromptOpen(false);
};

    const handleEmptyClick = () => {
      openConfirmationPrompt(); // Open the confirmation prompt
    };


  const handleConfirmEmpty = async (currentBatch: any) => {
    try {
      // Implement logic to send a request to updateDryer with a predefined body
      const emptyBatchData = {
        dryerId: currentBatch.dryerId,
        startDate: 'Tom',
        status: '',
        information: '',
        dryingTime: '',
        programTime: '',
        mc: '',
        dimension: '',
        endDate: '',
        dryingProgram: '',
      };
      await axios.put(
        'https://i8agl9rmn4.execute-api.eu-north-1.amazonaws.com/dev/updateDryer',
        emptyBatchData
      );
      await updateBatches(); // Update the batches list
      closeConfirmationPrompt(); // Close the confirmation prompt
    } catch (error) {
      console.error('Error emptying batch:', error);
    }
  };

  const handleCancelEmpty = () => {
    closeConfirmationPrompt(); // Close the confirmation prompt
  };


  return (
    <div className="dry-sheet-container">
      <div className={'title-bar'}>
        <h1 className={'title'}>Pågående torkningar</h1>
      </div>
      <div className={`view-toggle ${isGalleryView ? 'gallery-view' : ''}`}>
        <IconButton
          color={!isGalleryView ? 'default' : 'primary'}
          onClick={() => toggleView(true)}
        >
          <AppsIcon fontSize="large" />
          <Typography
            variant="caption"
            sx={{ fontSize: 'Large', marginLeft: '10px' }}
          >
            Översikts vy
          </Typography>
        </IconButton>
        <IconButton
          color={isGalleryView ? 'default' : 'primary'}
          onClick={() => toggleView(false)}
        >
          <AspectRatioIcon fontSize="large" />
          <Typography
            variant="caption"
            sx={{ fontSize: 'Large', marginLeft: '10px' }}
          >
            Mer Information
          </Typography>
        </IconButton>
      </div>
      <div className={`batch-grid ${isGalleryView ? 'gallery-view' : ''}`}>
        {batches.map((batch) => (
          <Paper
            key={batch.dryerId}
            style={statusToStyle[batch.status]}
            elevation={3}
            className={`batch-item`}
            onClick={() => handleBatchClick(batch)}
          >
            {isGalleryView ? (
              <div
                className={`gallery-item`}
                style={StatusToStyleMap[batch.status]}
              >
                {batch.dryerId}
              </div>
            ) : (
              <>
                <Typography variant="h5">{batch.dryerId}</Typography>
                <Typography>Status: {batch.status}</Typography>
                <Typography>Information: {batch.information}</Typography>
                <Typography>Drying Time: {batch.dryingTime}</Typography>
                <Typography>Program Time: {batch.programTime}</Typography>
                <Typography>MC: {batch.mc}</Typography>
                <Typography>Dimension: {batch.dimension}</Typography>
                <Typography>End Date: {batch.endDate}</Typography>
                <Typography>Program: {batch.dryingProgram}</Typography>
              </>
            )}
          </Paper>
        ))}
      </div>
      <Dialog open={isDialogOpen} onClose={handleCloseDialog}>
        <DialogContent>
          <IconButton
            edge="end"
            color="inherit"
            onClick={handleCloseDialog}
            aria-label="close"
            style={{ position: 'absolute', top: 10, right: 10 }}
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h5" sx={{ textDecoration: 'underline' }}>
            {selectedBatch?.dryerId}
          </Typography>
          <Typography>Status: {selectedBatch?.status}</Typography>
          <Typography>Information: {selectedBatch?.information}</Typography>
          <Typography>Drying Time: {selectedBatch?.dryingTime}</Typography>
          <Typography>Program Time: {selectedBatch?.programTime}</Typography>
          <Typography>MC: {selectedBatch?.mc}</Typography>
          <Typography>Dimension: {selectedBatch?.dimension}</Typography>
          <Typography>End Date: {selectedBatch?.endDate}</Typography>
          <Typography>Program: {selectedBatch?.dryingProgram}</Typography>
          <Button
            variant="contained"
            color="primary"
            style={{ margin: '10px 5px' }}
            onClick={handleStartTorkingClick} // Open BatchForm
          >
            Starta torkning
          </Button>
          <Button
            variant="contained"
            color="secondary"
            style={{ margin: '10px 5px' }}
            onClick={handleEmptyClick} // Pass the selected batch to empty
          >
            Töm
          </Button>
          <Dialog
            open={isConfirmationPromptOpen}
            onClose={closeConfirmationPrompt}
          >
            <DialogContent>
              <Typography variant="h5">
                Are you sure you want to empty the current batch?
              </Typography>
              <Button
                variant="contained"
                color="primary"
                style={{ margin: '10px 5px' }}
                onClick={() => handleConfirmEmpty(selectedBatch)}
              >
                Yes
              </Button>
              <Button
                variant="contained"
                color="secondary"
                style={{ margin: '10px 5px' }}
                onClick={handleCancelEmpty}
              >
                No
              </Button>
            </DialogContent>
          </Dialog>
          <IconButton edge="end" color="inherit" aria-label="settings">
            <SettingsIcon />
          </IconButton>
        </DialogContent>
      </Dialog>

      {/* BatchForm component */}
      <BatchForm
        isOpen={isBatchFormOpen}
        onClose={handleCloseBatchForm}
        onSubmit={async (modifiedData) => {
          // Implement logic to submit the batch update to the API
          // You can use the 'modifiedData' here to send the update
          // After successful submission, you can close the form
          try {
            // Implement logic to submit the batch update to the API
            // After successful submission, update the list of batches
            console.log('BATCHUPDATE : ', modifiedData);

            await axios.put(
              'https://i8agl9rmn4.execute-api.eu-north-1.amazonaws.com/dev/updateDryer',
              modifiedData
            ); // Replace with your API endpoint and data
            await updateBatches(); // Update the batches list
            setBatchFormOpen(false); // Close the BatchForm
          } catch (error) {
            console.error('Error submitting batch update:', error);
          }
          handleCloseBatchForm();
        }}
        batch={selectedBatch} // Pass the selectedBatch to BatchForm for editing
      />
    </div>
  );
};

export default DrySheetTable;

